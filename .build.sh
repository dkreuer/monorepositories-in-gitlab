#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DIR/.general.build.sh"
for subproj_funcs in $DIR/subprojects/*/.functions.build.sh; do
    source "$subproj_funcs"
done

TIMEFORMAT="Task completed in %3lR"
time task_${@:-default}
