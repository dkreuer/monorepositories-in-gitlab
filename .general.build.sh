#!/usr/bin/env bash
[[ "${BASH_SOURCE[0]}" != "${0}" ]] || (echo "THIS SCRIPT CANNOT BE RUN ALONE. PLEASE RUN build.sh FROM PROJECT ROOT." && exit 1)

if [ ! "$CI" != "" ]; then
    CI_COMMIT_TAG="SNAPSHOT"
    CI_JOB_ID=""
    CI_PROJECT_NAME="monorepositories-in-gitlab"
    CI_PROJECT_NAMESPACE="danielkreuer"
    CI_PROJECT_PATH_SLUG=$(echo -n "${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}" | tr -c [:alnum:] '-')
    CI_COMMIT_REF_SLUG=$(echo -n "${CI_COMMIT_TAG}" | tr -c [:alnum:] '-')
fi

function task_rebuild-gitlab-ci() {
    local TEMPLATE_BEFORE
    local TEMPLATE_AFTER
    local TEMPLATE_TASK
    local TASKS
    read -r -d '' TEMPLATE_TASK <<'EOF'
%TASK_NAME%:
    <<: *%TASK_TEMPLATE%
    variables:
        <<: *git_submodule_variables
        MONOREPO_CHANGED: "%CHANGED_CONDITION%"
EOF

    for subproj in $DIR/subprojects/*; do
        [[ -e "$subproj/.dependencies.build.sh" ]] && source "$subproj/.dependencies.build.sh"
        [[ -e "$subproj/.functions.build.sh" ]] && source "$subproj/.functions.build.sh"

        CURRENT_FUNCS=$(compgen -A function | grep task_ | grep :$(basename $subproj) | sed s/task_//g)

        for taskname in $CURRENT_FUNCS; do
            TASKS=$(echo -e "$TASKS"; sed "
                s#%TASK_NAME%#${taskname}#g;
                s#%TASK_TEMPLATE%#${taskname%:$(basename $subproj)}_template#g;
                s#%CHANGED_CONDITION%#subprojects/$(basename $subproj)|.*build.sh${MONOREPO_CHANGED_DEPS:+|}${MONOREPO_CHANGED_DEPS}#g;
            " <<< "${TEMPLATE_TASK}")
        done
    done

    read -r -d '' TEMPLATE_BEFORE <<'EOF'
stages:
  - before_test
  - test
  - after_test
  - before_build
  - build
  - after_build
  - before_package
  - package
  - after_package
  - before_deploy
  - deploy
  - after_deploy
  - notify

.root_template: &root_template
    image: alpine:3.6
    dependencies: []
    before_script:
        - '[[ "$PRIVATE_TOKEN" == "" ]] && echo "No PRIVATE_TOKEN has been set as build variable. Cannot continue." && exit 5'
        - apk --no-cache add bash curl jq git
        - CI_SERVER_URL=$CI_PROJECT_URL .monorepo.gitlab/last_green_commit.sh
    script: |
        [ "${MONOREPO_CHANGED}" != "" ] && .monorepo.gitlab/build_if_changed.sh "${MONOREPO_CHANGED}|build.sh" ":"
        if [ ! -f .SKIP_THIS_JOB ]; then
            bash ./.build.sh ${CI_JOB_NAME}
        fi
    artifacts:
        paths:
        - ".SKIP_THIS_JOB"
        expire_in: 24h
    variables: &git_submodule_variables
        GIT_SUBMODULE_STRATEGY: recursive

.test_template: &test_template
  <<: *root_template
  stage: test

.build_template: &build_template
  <<: *root_template
  stage: build

.package_template: &package_template
  <<: *root_template
  stage: package

.deploy_template: &deploy_template
  <<: *root_template
  stage: deploy

.notify_template: &notify_template
  stage: notify
  image: alpine:3.6

EOF

    read -r -d '' TEMPLATE_AFTER <<'EOF'
notify_success:
  <<: *notify_template
  when: on_success
  script: |
    echo "notify on SUCCESS"

notify_failure:
  <<: *notify_template
  when: on_failure
  script: |
    echo "notify on FAILURE"
EOF

    echo "$TEMPLATE_BEFORE" > "$DIR/.gitlab-ci.yml"
    echo "$TASKS" >> "$DIR/.gitlab-ci.yml"
    echo "$TEMPLATE_AFTER" >> "$DIR/.gitlab-ci.yml"
}
function task_default() {
    help
}
function help {
    echo "$0 <task> <args>"
    echo "Tasks:"
    compgen -A function | grep task_ | sed s/task_//g | cat -n
}
