#!/usr/bin/env bash
[[ "${BASH_SOURCE[0]}" != "${0}" ]] || (echo "THIS SCRIPT CANNOT BE RUN ALONE. PLEASE RUN build.sh FROM PROJECT ROOT." && exit 1)

function task_build:frontend() {
    echo "task_build:frontend"
}

function task_deploy:frontend() {
    echo "task_deploy:frontend"
}
