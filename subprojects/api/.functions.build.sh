#!/usr/bin/env bash
[[ "${BASH_SOURCE[0]}" != "${0}" ]] || (echo "THIS SCRIPT CANNOT BE RUN ALONE. PLEASE RUN build.sh FROM PROJECT ROOT." && exit 1)

function task_test:api() {
    echo "task_test:api"
}

function task_build:api() {
    echo "task_build:api"
}

function task_package:api() {
    echo "task_package:api"
}

function task_deploy:api() {
    echo "task_deploy:api"
}
