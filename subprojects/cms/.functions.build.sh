#!/usr/bin/env bash
[[ "${BASH_SOURCE[0]}" != "${0}" ]] || (echo "THIS SCRIPT CANNOT BE RUN ALONE. PLEASE RUN build.sh FROM PROJECT ROOT." && exit 1)

function task_build:cms() {
    echo "task_build:cms"
}

function task_package:cms() {
    echo "task_package:cms"
}

function task_deploy:cms() {
    echo "task_deploy:cms"
}
