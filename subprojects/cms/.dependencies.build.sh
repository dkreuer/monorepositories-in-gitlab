#!/usr/bin/env bash
[[ "${BASH_SOURCE[0]}" != "${0}" ]] || (echo "THIS SCRIPT CANNOT BE RUN ALONE. PLEASE RUN build.sh FROM PROJECT ROOT." && exit 1)

MONOREPO_CHANGED_DEPS=subprojects/frontend
