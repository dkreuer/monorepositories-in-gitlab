#!/usr/bin/env bash
[[ "${BASH_SOURCE[0]}" != "${0}" ]] || (echo "THIS SCRIPT CANNOT BE RUN ALONE. PLEASE RUN build.sh FROM PROJECT ROOT." && exit 1)

function task_build:docs() {
    echo "task_build:docs"
}

function task_package:docs() {
    echo "task_package:docs"
}
